package se.rende.klearsky.weatherservices

import org.apache.commons.io.FileUtils
import org.junit.Test
import se.rende.klearsky.utils.WireMockTest
import java.io.File
import kotlin.test.assertEquals
import kotlin.test.assertNotNull

class SmhiClientTest: WireMockTest() {

    @Test
    fun shouldParseWeatherDataIntoForecast() {
        val responseBody = FileUtils.readFileToString(File(javaClass.getResource("/smhi_data.json").toURI().path), "utf-8")
        setupMockOkResponse("/api/category/pmp3g/version/2/geotype/point/lon/16/lat/58/data.json", responseBody)
        val forecast = SmhiClient("http://localhost:1337").getTodaysForecast()
        assertNotNull(forecast)
        assertEquals(10.1f, forecast.minTemp)
        assertEquals(21.1f, forecast.maxTemp)
        assertEquals(false, forecast.riskOfRain)
        assertEquals("SMHI", forecast.provider)
        assertEquals(-1, forecast.rainProbability)
        assertEquals(12, forecast.thunderProbability)
    }
}