package se.rende.klearsky.weatherservices

import org.apache.commons.io.FileUtils
import org.junit.Test
import se.rende.klearsky.utils.WireMockTest
import java.io.File
import kotlin.test.assertEquals

class YrClientTest: WireMockTest() {

    @Test
    fun shouldParseWeatherNoDataIntoForecast() {
        val responseBody = FileUtils.readFileToString(File(javaClass.getResource("/yrno_data.xml").toURI().path), "utf-8")
        setupMockOkResponse("/place/Sweden/Stockholm/Stockholm/forecast_hour_by_hour.xml", responseBody)
        val forecast = YrClient("http://localhost:1337").getTodaysForecast()
        assertEquals(23f, forecast.maxTemp)
        assertEquals(14f, forecast.minTemp)
        assertEquals(0, forecast.rainProbability)
        assertEquals(false, forecast.riskOfRain)
        assertEquals("yr.no", forecast.provider)
        assertEquals(0, forecast.rainProbability)
        assertEquals(-1, forecast.thunderProbability)
    }
}