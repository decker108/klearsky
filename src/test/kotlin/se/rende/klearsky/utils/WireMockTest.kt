package se.rende.klearsky.utils

import com.github.tomakehurst.wiremock.client.WireMock
import com.github.tomakehurst.wiremock.core.WireMockConfiguration
import com.github.tomakehurst.wiremock.junit.WireMockRule
import org.junit.Rule

open class WireMockTest {

    @Rule
    @JvmField
    val wireMockRule = WireMockRule(WireMockConfiguration.options().port(1337))

    fun setupMockOkResponse(url: String, responseBody: String?) {
        wireMockRule.stubFor(WireMock.get(WireMock.urlPathEqualTo(url))
                .willReturn(WireMock.ok(responseBody)))
    }

    fun setupMock500Response(url: String, clazz: Class<Any>) {
        val uri = clazz.getResource(url).toURI()
        wireMockRule.stubFor(WireMock.get(WireMock.urlPathMatching(url))
                .willReturn(WireMock.serverError()))
    }
}