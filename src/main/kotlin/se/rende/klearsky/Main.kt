package se.rende.klearsky

import mu.KLogger
import mu.KLogging
import se.rende.klearsky.notifications.Sender
import se.rende.klearsky.notifications.TelegramSender
import se.rende.klearsky.weatherservices.*
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit

fun main() {
    val logger = KLogging().logger()
    logger.info("Checking weather forecasts...")
    try {
        doWork(logger, TelegramSender())
    } catch (e: Exception) {
        logger.error("Unexpected error", e)
    }
    logger.info("Done checking")
}

fun doWork(logger: KLogger, sender: Sender) {
    val executor = Executors.newFixedThreadPool(3)
    try {
        val weatherClients: List<WeatherDataClient> = listOf(
                SmhiClient()
                , YrClient()
    //            , MetOfficeClient()
        )
        val futures = weatherClients.map { client: WeatherDataClient ->
            executor.submit<Forecast> { client.getTodaysForecast() } }
        executor.awaitTermination(5, TimeUnit.SECONDS)

        val forecasts = futures.map { it.get() }
        if (forecasts.any { it.riskOfRain }) {
            sender.sendMessage(forecasts.joinToString(transform = { it.toString() }, separator = "\n"))
            logger.info("Bad weather incoming...")
        } else {
            logger.info("Clear skies!")
        }
    } finally {
        executor?.shutdown()
    }
}