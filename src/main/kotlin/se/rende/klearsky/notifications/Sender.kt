package se.rende.klearsky.notifications

interface Sender {
    fun sendMessage(message: String)
}
