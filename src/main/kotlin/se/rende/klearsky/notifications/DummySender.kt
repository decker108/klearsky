package se.rende.klearsky.notifications

import mu.KLogging

class DummySender: Sender, KLogging() {
    val messages: MutableList<String> = mutableListOf()

    override fun sendMessage(message: String) {
        messages.add(message)
        logger.info(message)
    }
}