package se.rende.klearsky.weatherservices

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.fasterxml.jackson.module.kotlin.readValue
import mu.KLogging
import org.apache.http.client.methods.HttpGet
import org.apache.http.impl.client.HttpClients
import java.io.ByteArrayOutputStream

@Suppress("unused", "UNUSED_PARAMETER")
private enum class WeatherSymbols(id: Int) {
    CLEAR_SKY(1),
    NEARLY_CLEAR_SKY(2),
    VARIABLE_CLOUDINESS(3),
    HALFCLEAR_SKY(4),
    CLOUDY_SKY(5),
    OVERCAST(6),
    FOG(7),
    LIGHT_RAIN_SHOWERS(8),
    MODERATE_RAIN_SHOWERS(9),
    HEAVY_RAIN_SHOWERS(10),
    THUNDERSTORM(11),
    LIGHT_SLEET_SHOWERS(12),
    MODERATE_SLEET_SHOWERS(13),
    HEAVY_SLEET_SHOWERS(14),
    LIGHT_SNOW_SHOWERS(15),
    MODERATE_SNOW_SHOWERS(16),
    HEAVY_SNOW_SHOWERS(17),
    LIGHT_RAIN(18),
    MODERATE_RAIN(19),
    HEAVY_RAIN(20),
    THUNDER(21),
    LIGHT_SLEET(22),
    MODERATE_SLEET(23),
    HEAVY_SLEET(24),
    LIGHT_SNOWFALL(25),
    MODERATE_SNOWFALL(26),
    HEAVY_SNOWFALL(27)
}


/**
msl 	    hPa 	    hmsl 	0 	Air pressure 	                        Float, 1 dec
t 	        C 	        hl 	    2 	Air temperature 	                    Float, 1 dec
vis 	    km 	        hl 	    2 	Horizontal visibility 	                Float, 1 dec
wd 	        degree 	    hl 	    10 	Wind direction 	                        Int
ws 	        m/s 	    hl 	    10 	Wind speed 	                            Float, 1 dec
r 	        % 	        hl 	    2 	Relative humidity 	                    Int, 0-100
tstm 	    % 	        hl 	    0 	Thunder probability 	                Int, 0-100
tcc_mean 	octas 	    hl 	    0 	Mean value of total cloud cover 	    Int, 0-8
lcc_mean 	octas 	    hl 	    0 	Mean value of low level cloud cover 	Int, 0-8
mcc_mean 	octas 	    hl 	    0 	Mean value of medium level cloud cover 	Int, 0-8
hcc_mean 	octas 	    hl 	    0 	Mean value of high level cloud cover 	Int, 0-8
gust 	    m/s 	    hl 	    10 	Wind gust speed 	                    Float, 1 dec
pmin 	    mm/h 	    hl 	    0 	Minimum precipitation intensity 	    Float, 1 dec
pmax 	    mm/h 	    hl 	    0 	Maximum precipitation intensity 	    Float, 1 dec
spp 	    % 	        hl 	    0 	Percent of precipitation in frozen form Int, -9 or 0-100
pcat 	    category 	hl 	    0 	Precipitation category 	                Int, 0-6
pmean 	    mm/h 	    hl 	    0 	Mean precipitation intensity 	        Float, 1 dec
pmedian 	mm/h 	    hl 	    0 	Median precipitation intensity 	        Float, 1 dec
Wsymb2 	    code 	    hl 	    0 	Weather symbol 	                        Int, 1-27
 */
private data class SmhiTimePoint(val validTime: Any?, val parameters: List<Map<String, Any>>)

@JsonIgnoreProperties(ignoreUnknown = true)
private data class SmhiResponse(val approvedTime: Any?,
                                val referenceTime: Any?,
                                val geometry: Any?,
                                val timeSeries: List<SmhiTimePoint>)

class SmhiClient(
        private val apiHost: String = "https://opendata-download-metfcst.smhi.se"
): WeatherDataClient, KLogging() {

    override fun getTodaysForecast(): Forecast {
        val mapper = ObjectMapper()
                .registerModule(Jdk8Module())
                .registerModule(JavaTimeModule())
                .registerModule(KotlinModule())
                .enable(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
                .enable(SerializationFeature.WRITE_DATES_WITH_ZONE_ID)

        HttpClients.createDefault().use { client ->
            val url = "$apiHost/api/category/pmp3g/version/2/geotype/point/lon/16/lat/58/data.json"
            logger.info("Calling SMHI API with with url: $url")
            client.execute(HttpGet(url)).use { httpResponse ->
                if (httpResponse.statusLine.statusCode == 200) {
                    val response = mapper.readValue(ByteArrayOutputStream().use { baos ->
                                    httpResponse.entity.writeTo(baos)
                                    String(baos.toByteArray())
                                }) as SmhiResponse

                    val riskOfRain = response.timeSeries.subList(0, 12).map { timePoint ->
                        timePoint.parameters.find { it["name"] == "Wsymb2" }.orEmpty()["values"] ?: emptyList<Int>().first() }
                            .any { it in 8..21 }
                    val temperatures = response.timeSeries.subList(0, 12).map { timePoint ->
                        timePoint.parameters.first { it["name"] == "t" }["values"] ?: emptyList<Float>().first() }.map { (it as List<Float>).first() }
                    val thunderProbability = response.timeSeries.subList(0, 12).map { timePoint ->
                        timePoint.parameters.first { it["name"] == "tstm" }["values"] ?: emptyList<Int>()}.map{(it as List<Int>).first()}.max() ?: 0
                    return Forecast(
                            "SMHI",
                            temperatures.min() ?: 0f,
                            temperatures.max() ?: 0f,
                            riskOfRain,
                            -1,
                            thunderProbability
                    )
                } else {
                    val errorMsg = "Error calling SMHI: ${httpResponse.statusLine.statusCode} ${httpResponse.statusLine.reasonPhrase}"
                    throw IllegalStateException(errorMsg)
                }
            }
        }
    }
}