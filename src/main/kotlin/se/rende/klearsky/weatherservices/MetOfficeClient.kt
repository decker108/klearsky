package se.rende.klearsky.weatherservices

import mu.KLogging

class MetOfficeClient(
        private val apiHost: String = "http://wow.metoffice.gov.uk/automaticreading?"
): WeatherDataClient, KLogging() {
    override fun getTodaysForecast(): Forecast {
        return Forecast("", 0f, 0f, false, 0, 0)
    }
}