package se.rende.klearsky.weatherservices

import com.fasterxml.jackson.dataformat.xml.XmlMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.fasterxml.jackson.module.kotlin.readValue
import mu.KLogging
import org.apache.http.client.methods.HttpGet
import org.apache.http.impl.client.HttpClients
import java.io.ByteArrayOutputStream
import java.io.File
import java.util.concurrent.ExecutorService
import java.util.concurrent.Future

private data class YrTimePoint(val from: String,
                               val to: String,
                               val symbol: Map<String, String>,
                               val precipitation: Map<String, String>,
                               val windDirection: Map<String, String>,
                               val windSpeed: Map<String, String>,
                               val temperature: Map<String, String>,
                               val pressure: Map<String, String>)

private data class YrResponse(
        val location: Any?,
        val credit: Any?,
        val links: Any?,
        val meta: Any?,
        val sun: Any?,
        val forecast: List<List<YrTimePoint>>
)

class YrClient(
        private val apiHost: String = "https://www.yr.no"
) : WeatherDataClient, KLogging() {

    override fun getTodaysForecast(): Forecast {
        val mapper = XmlMapper().registerModule(KotlinModule())
        val url = "$apiHost/place/Sweden/Stockholm/Stockholm/forecast_hour_by_hour.xml"

        HttpClients.createDefault().use { client ->
            logger.info("Calling Yr API with with url: $url")
            client.execute(HttpGet(url)).use { httpResponse ->
                if (httpResponse.statusLine.statusCode == 200) {
                    val yrResponse = mapper.readValue(ByteArrayOutputStream().use { baos ->
                        httpResponse.entity.writeTo(baos)
                        String(baos.toByteArray())
                    }) as YrResponse

                    val temperatures = yrResponse.forecast
                            .first()
                            .subList(0, 12)
                            .map { it.temperature.getValue("value").toFloat() }
                    val rainProbabilities = yrResponse.forecast
                            .first()
                            .subList(0, 12)
                            .map { it.precipitation.getValue("value").toInt() }
                    return Forecast(
                            "yr.no",
                            temperatures.min()!!,
                            temperatures.max()!!,
                            rainProbabilities.max() ?: 0 > 5,
                            rainProbabilities.max() ?: 0,
                            -1
                    )
                } else {
                    val errorMsg = "Error calling Yr.no: ${httpResponse.statusLine.statusCode} ${httpResponse.statusLine.reasonPhrase}"
                    throw IllegalStateException(errorMsg)
                }
            }
        }
    }
}