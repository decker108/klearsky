package se.rende.klearsky.weatherservices

data class Forecast(
        val provider: String,
        val minTemp: Float,
        val maxTemp: Float,
        val riskOfRain: Boolean,
        val rainProbability: Int,
        val thunderProbability: Int
) {
    override fun toString(): String {
        return """
            $provider:s väderleksrapport
            Lägsta temperatur: $minTemp
            Högsta temperatur: $maxTemp
            Regnrisk/regnmängd: $rainProbability
            Åskrisk: $thunderProbability
        """.trimIndent()
    }
}