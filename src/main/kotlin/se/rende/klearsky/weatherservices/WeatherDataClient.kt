package se.rende.klearsky.weatherservices

interface WeatherDataClient {
    fun getTodaysForecast(): Forecast
}