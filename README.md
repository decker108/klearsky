# Klearsky

A weather notification service written in Kotlin that uses data from SMHI and Yr.no.
Uses Telegram or Signal for notifications. Scheduled by crontab.

Data source credits:
* [Weather forecast from Yr, delivered by the Norwegian Meteorological Institute and the NRK](http://www.yr.no/place/Sweden/Stockholm/Stockholm/)
* [SMHI](https://www.smhi.se/klimatdata/Oppna-data/Information-om-oppna-data/villkor-for-anvandning-1.30622)